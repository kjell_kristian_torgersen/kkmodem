/*
 * IncFile1.h
 *
 * Created: 01.02.2015 11:10:28
 *  Author: Kjell
 */ 


#ifndef UART_H_
#define UART_H_

#define UARRX_BUFF_SIZE 128
typedef struct {
	unsigned char Data[UARRX_BUFF_SIZE];
	unsigned char Head;
	unsigned char Tail;
} UARTBuf_t;

int UART_peekchar();
int UART_getchar();
void UART_init(unsigned long baudrate);
void UART_putchar(char c);

#endif /* USART_H_ */