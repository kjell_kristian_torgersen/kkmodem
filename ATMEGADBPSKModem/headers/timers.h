/*
 * timers.h
 *
 * Created: 10.12.2015 20:55:24
 *  Author: Kjell
 */ 


#ifndef TIMERS_H_
#define TIMERS_H_

void TIMER_Init(void);
uint16_t TIMER_GetSeconds(void);
uint16_t TIMER_GetMilliSeconds(void);

#endif /* TIMERS_H_ */