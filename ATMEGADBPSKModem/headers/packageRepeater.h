/*
 * packageRepeater.h
 *
 * Created: 26.02.2015 20:25:12
 *  Author: Kjell-Kristian
 */ 


#ifndef PACKAGEREPEATER_H_
#define PACKAGEREPEATER_H_

#define PACKAGE_MAX_SIZE 64

typedef enum RepeaterStateTag {RS_WAITFORPREAMBLE, RS_GETSIZE, RS_GETDATA} RepeaterState_t;

void repeaterProcessChar(char c);
void repeaterAutoTransmit(void);

#endif /* PACKAGEREPEATER_H_ */