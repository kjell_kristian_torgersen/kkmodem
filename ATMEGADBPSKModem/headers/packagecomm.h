/*
 * packagecomm.h
 *
 * Created: 10.12.2015 21:08:24
 *  Author: Kjell
 */ 


#ifndef PACKAGECOMM_H_
#define PACKAGECOMM_H_

typedef enum PackageTag {
	PT_GET = 0, PT_SET = 1, PT_DATA = 2, PT_CONFIRM = 3
} PackageType_t;

typedef enum PackageValueTag {PV_NONE } PackageValue_t;

#define START_BYTE 0xAA
#define PACKAGE_SIZE 32
#define PACKAGE_MASK (PACKAGE_SIZE-1)

void PC_transmitDataPackage(PackageValue_t value, uint8_t * payload, uint8_t length);
void PC_processPackage(uint8_t b);
void PC_transmitMessage(char *message);


#endif /* PACKAGECOMM_H_ */