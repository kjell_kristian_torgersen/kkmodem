/*
 * io_pins.h
 *
 * Created: 11.12.2015 22:04:38
 *  Author: Kjell
 */ 


#ifndef IOPINS_H_
#define IOPINS_H_

#define MODEM_TX_DDR (DDRD)
#define MODEM_TX_PIN (1<<6)

#define MODEM_PTT_DDR (DDRD)
#define MODEM_PTT_PORT (PORTD)
#define MODEM_PTT_PIN (1<<2)



#endif /* IO-PINS_H_ */