/*
 * packageRepeater.c
 *
 * Created: 26.02.2015 20:25:28
 *  Author: Kjell-Kristian
 */ 

#include "headers/global.h"
#include "headers/packageRepeater.h"
#include <stdint.h>

RepeaterState_t repeater_state = RS_WAITFORPREAMBLE;

char package[PACKAGE_MAX_SIZE]; // buffer of received package
int package_index = 0; // current index reading or writing
int package_size = 0; // package size in bytes

Bool dataToTransmit = FALSE;
extern volatile int16_t new_tx_word;

int calculateChecksum(char *data, int words)
{
	int i;
	unsigned int checksum = 0;
	for(i=0; i < words; i++) {
		checksum += ((unsigned int)data[2 * i]) + ((unsigned int)data[2 * i + 1] << 8);
	}
	return ~checksum;
}

void repeaterProcessChar(char c)
{
	switch(repeater_state) {
		case RS_WAITFORPREAMBLE: // wait for preamble
		if(c == 0xAA) {
			package_index = 0;
			package[package_index++] = c;
			repeater_state = RS_GETSIZE;
		}
		break;
		case RS_GETSIZE: // get package size
		package[package_index++] = c;
		package_size = (package[1] << 1) + 4; // (number of words)*2 + preamble + two checksum bytes
		if(package_size > PACKAGE_MAX_SIZE) package_size = PACKAGE_MAX_SIZE;
		repeater_state = RS_GETDATA;
		break;
		case RS_GETDATA: // collect data
		package[package_index++] = c;
		if(package_index >= package_size) {
			if(calculateChecksum(package, package_size >> 1) == 0) {
				package_index = 0;
				dataToTransmit = TRUE;
			}
			repeater_state = RS_WAITFORPREAMBLE;
		}
		break;
	}
}

void repeaterAutoTransmit(void) 
{
	if(dataToTransmit) {
		if(new_tx_word == -1) {
			int _new_tx_word;
			_new_tx_word = 0xFF00;
			_new_tx_word |= ((package[package_index++]) & 0xFF);
			_new_tx_word <<= 1;
			_new_tx_word &= ~0x1; // Stop bit
			new_tx_word = _new_tx_word;
			if(package_index >= package_size) dataToTransmit = FALSE;
		}
	}
}