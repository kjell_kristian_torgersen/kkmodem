/*
 * USART.c
 *
 * Created: 01.02.2015 11:01:45
 *  Author: Kjell
 */ 

#include <avr/interrupt.h>
#include "global.h"
#include "UART.h"

UARTBuf_t uartRxQ;

int UART_peekchar() 
{
		int ret = -1;
		if(uartRxQ.Head != uartRxQ.Tail) {
			ret = uartRxQ.Data[uartRxQ.Tail];
		}

		return ret;
}

int UART_getchar()
{
	int ret = -1;
	if(uartRxQ.Head != uartRxQ.Tail) {
		ret = uartRxQ.Data[uartRxQ.Tail];
		uartRxQ.Tail = (uartRxQ.Tail + 1) & (UARRX_BUFF_SIZE -1);
	}

	return ret;
}

// Send en karakter med HW. UART
void UART_putchar(char c)
{
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = c;
}

// Initialiser UART med baudrate spesifisert av bruker. Registerverdi beregnes.
void UART_init(unsigned long baudrate)
{
	uartRxQ.Head = uartRxQ.Tail = 0;
	cli(); // Disable interrupts
	UBRR0 = (unsigned int)(16000000UL/(16UL*(unsigned long)baudrate)-1);
	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0); // Skru p� sendekrets, mottakskrets og mottaksinterrupt.
	UCSR0C = (0<<USBS0)|(3<<UCSZ00); // 8 bits, 1 stop bit
	sei(); // Enable interrupts
}

// HW. UART mottaksinterrupt. Mottar karakterer fra bruker og legger i buffer for sending med modem.
ISR(USART_RX_vect)
{
	//volatile char dummy;
	
	uartRxQ.Data[uartRxQ.Head] = UDR0;
	uartRxQ.Head = (uartRxQ.Head + 1) & (UARRX_BUFF_SIZE - 1);

}