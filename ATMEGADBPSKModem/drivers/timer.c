#include <stdint.h>
#include <avr/interrupt.h>
#include <avr/io.h>

static uint16_t TIMER_s = 0;
static uint16_t TIMER_ms = 0;
static uint16_t TIMER_ms2 = 0;

void TIMER_Init(void) 
{
	//DDRB |= (1 << PORTB5);
	TCCR1B |= (1 << WGM12);
	OCR1A = 15999; //et mssekund
	TIMSK1 |= (1 << OCIE1A);
	TCCR1B |= ((1 << CS10) | (0 << CS12)); // 16MHz
}

uint16_t TIMER_GetSeconds(void)
{
	uint16_t ret;
	cli();
	ret = TIMER_s;
	sei();
	return ret;
}

uint16_t TIMER_GetMilliSeconds(void)
{
	uint16_t ret;
	cli();
	ret = TIMER_ms;
	sei();
	return ret;
}

ISR(TIMER1_COMPA_vect) 
{
	TIMER_ms++; //Et ms sekund har passert.
	TIMER_ms2++;
	if (TIMER_ms2 >= 1000) {
		TIMER_s++;
		TIMER_ms2 = 0;
	}
}