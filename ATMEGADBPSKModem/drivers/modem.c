/*
* modem.c
*
* Created: 07.02.2015 15:36:23
*  Author: Kjell
*/
#include "global.h"
#include "avr/io.h"
#include "avr/interrupt.h"
#include "avr/pgmspace.h" // for � kunne oppbevare data (her symbolet vi skal sende i program-minnet)

/*
Shift	Baud	Samples/symb.
*     2	9600			(4)
*     3	4800			(8)
*	  4	2400			(16)
*     5	1200			(32) * Anbefalt for st�yimmunitet
*     6	 600			(64) * Anbefalt for st�yimmunitet
*/

// Bruk denne define for � velge baud rate, se tabell over. Husk � Initialiser HW. UART til � minst ha denne baudrate eller h�yere.
#define BAUDRATE_SHIFT 5

// Denne regner ut hvor mange samples det blir per symbol
#define SAMPLES (1<<BAUDRATE_SHIFT)

// Tabell for � generere en sinussvingning med 64 punkter @ 8 bit.
/*const unsigned char sym[64] PROGMEM = {
127, 139, 152, 164, 176, 187, 198, 208, 217, 226, 233, 239, 245, 249,
252, 254, 255, 254, 252, 249, 245, 239, 233, 226, 217, 208, 198, 187,
176, 164, 152, 139, 127, 115, 102, 90, 78, 67, 56, 46, 37, 28, 21, 15,
9, 5, 2, 0, 0, 0, 2, 5, 9, 15, 21, 28, 37, 46, 56, 67, 78, 90, 102, 115,
};*/

const unsigned char sym[64] PROGMEM = {
128, 146, 165, 182, 198, 213, 226, 237, 245, 251, 254, 255, 253, 248, 
240, 230, 218, 203, 188, 170, 152, 134, 115, 97, 79, 62, 47, 33, 21, 12, 
5, 1, 0, 1, 5, 12, 21, 33, 47, 62, 79, 97, 115, 134, 152, 170, 188, 203, 
218, 230, 240, 248, 253, 255, 254, 251, 245, 237, 226, 213, 198, 182, 165, 146
};

#define FREQ_UP 0

// Buffer for � mellomlagre mottatte samples, plass til ett helt symbol
signed char inputBuffer[SAMPLES];

// Produktbuffer for ett helt symbol
int16_t prodBuffer1[SAMPLES];

// Akkumulator brukt for � implementere lav pass filter
int16_t accumulator1 = 0;

// Indeks for lesning/skriving til mottaksbuffer
uint16_t idx=0;
// Indeks for lesning/skriving til produktbuffer
uint16_t idxp=0;

// Terskel som fungerer likt som squelch p� mottak. For lav, ta inn st�y som karakterer, for h�y, ikke slipp gjennom gyldige karakterer.
#define THRESHOLD 16

// Brukt for � telle hvor langt en er uti ett symbol under sending
int sw = 0;

// Neste ord en �nsker � sende. Ord brukes da vi sender 10 bit om gangen start bit (alltid lav), databits (8 stykk) deretter stoppbit (h�y).
int16_t new_tx_word = -1;

// Ord som sendes n�
uint16_t tx_word = 0xFFFF;

// Bit index i ord som sendes
uint16_t tx_idx = 0;

// Brukt for � holde oversikt over om vi sendte h�y eller lav sist, for denne m� alterneres for � sende 0 og holdes til samme verdi for � sende 1
unsigned char currentBit = 0;

//unsigned char tx = 0;

// Prototype for mottatt signal software UART dekoder.
void MODEM_Decoder(signed int bit);

// Hvilken karakter som skal sendes
unsigned int tx_char = 0;

// ADC interrupt som brukes til b�de mottak og sending
ISR(ADC_vect)
{
	PORTD |= (1<<7); // Sett port D pinne 7 h�y for � kunne m�le tiden som blir brukt i interruptet
	
	char oldestSample = inputBuffer[idx]; // Les ut bakerste sample, som er n�yaktig ett symbol tilbake i tid
	
	inputBuffer[idx] = (ADCH-128); // Les inn n� verdi fra ADC
	
	accumulator1 -= (prodBuffer1[idxp]>>(BAUDRATE_SHIFT-0)); // Trekk fra eldste produktbufferverdi
	prodBuffer1[idxp] = (int16_t)(signed char)oldestSample*(int16_t)(signed char)inputBuffer[idx]; // Multipliser nyeste sample vi har med eldste sampel vi har
	accumulator1 += (prodBuffer1[idxp]>>(BAUDRATE_SHIFT-0)); // Legg til nyeste produktbufferverdi
	
	// Akkumulatoren inneholder n� ett demodulert signal. Dette fordi at signalet som kom inn har blitt multiplisert med segselv forsinket med ett symbol, deretter glidende gjennomsnitt av et halvt symbol.
	// Da gj�res en enkel UART dekoding p� dette
	MODEM_Decoder(accumulator1);

	// Da er vi ferdige med mottak, s� fra n� av foreg�r sending stort sett
	//if(tx) {
	// Sjekk om vi holder p� � sende det ene eller andre symbolet (med eller uten 180 graders faseforskyving)
	if(currentBit & 1) { // Ikke 180 graders faseforskyving.
		OCR0A = pgm_read_byte(&sym[(sw<<FREQ_UP) & 63]);
		} else { // 180 graders faseforskyvning implementert med subtraksjon
		OCR0A = 255 - pgm_read_byte(&sym[(sw<<FREQ_UP) & 63]);
	}
	// Inkrementer teller slik at vi f�r ut symbol med riktig baudrate. Her er det mulig � sende h�yere frekvenser ved � gange opp, dersom en �nsker en h�yere b�reb�lge. (F.eks. ha 600 bit/s baud rate med 2400 Hz b�reb�lge. Da vil en bruke 2400 Hz +/- 600 Hz)
	sw += 1 << (6-BAUDRATE_SHIFT);
	
	// 	Finn hvilket symbol vi skal sende dersom vi er ferdige med n�v�rende symbol
	if(sw >= (64)) {
		// Nullstill � sjekk om vi skal sende en 0. For � sende nuller m� vi skifte 180 grader.
		sw = 0;
		if((tx_word & 1) == 0) {
			currentBit ^= 1;
		}
		// Gj�r klart for � plukke neste bit neste gang
		tx_word >>= 1;
		
		// Tell opp hvor mange bit vi har sendt
		tx_idx++;
		
		// Dersom vi har sendt 10 bit, plukk neste karakter dersom den er klar, eller send ett 10 bit ord med h�y (standard UART � sende h�y dersom en ikke har karakterer)
		if(tx_idx >= 10) {
			// Ligger ett nytt ord klart?
			if(new_tx_word != -1) { // Ja
				tx_word = new_tx_word; // Kopier inn i sende buffer
				new_tx_word = -1; // Slett
				} else { // Send bare h�y (ren b�reb�lge)
				tx_word = 0xFFFF;
			}
			// Nullstill bit teller
			tx_idx = 0;
		}
	}
	// Inkrementer bufferteller til neste gang for mottak, wrap rundt dersom det trengs
	idx++;
	if(idx >= SAMPLES) idx = 0;
	idxp++;
	if(idxp >= SAMPLES) idxp = 0;
	//}
	// Sett port D pinne 7 lav igjen. Er n� ferdig.
	PORTD &= ~(1<<7);
}

// Uart dekoder tilstand
unsigned char DECODER_state = 0;

// Teller brukt i UART dekoder
unsigned char DECODER_ctr = 0;

// Mottatt ord frem til n�
unsigned int DECODER_rxword = 0;

// Antall bit mottatt frem til n�
unsigned char DECODER_rxbits = 0;

// Ferdig mottatt karakter som kan slettes etter innlesning for � f� neste.
volatile signed int DECODER_rxchar = -1;

void MODEM_Decoder(signed int bit)
{
	switch(DECODER_state) {
		case 0: // Vent p� startbit
		if(bit < -THRESHOLD) { // Startbit passerer terskel, hopp til neste tilstand for � validere
			DECODER_state = 1;
			DECODER_ctr = 1;
		}
		break;
		case 1: // Validering av startbit, sjekk at vi har halve startbitet i n�rheten av 0 eller lavere.
		DECODER_ctr++;
		if(DECODER_ctr >= SAMPLES/2) { // Start bit OK. Hopp til neste tilstand for � sample databit
			DECODER_rxbits = 0; // Nullstill variabler
			DECODER_ctr = 1;
			DECODER_rxword = 0;
			DECODER_state = 2;
		}
		if(bit > THRESHOLD) { // Start bitet var ikke godt nok. Hopp tilbake til tilstanden for � vente p� nytt startbit
			DECODER_state = 0;
		}
		break;
		case 2: // Samle databittene
		DECODER_ctr++; // Tell opp en symbol periode. N� startet vi midt i forrige bit, � vil ende i midten av neste bit
		if(DECODER_ctr >= SAMPLES) {
			DECODER_ctr = 0; // Nullstill teller � lagre unna mottatt bit
			DECODER_rxword >>= 1; // Lag plass til neste bit
			DECODER_rxword |= (bit<0) ? 0 : 0x200; // Legg til bit dersom 1
			DECODER_rxbits++; // Tell antall mottatte bit
			if(DECODER_rxbits >= 9) { // Dersom 9 bit, valider stoppbit
				if(DECODER_rxword & 0x200) { // stoppbit er ok
					DECODER_rxword >>= 1; // fjern stoppbit
					DECODER_rxchar = DECODER_rxword; // kopier ut mottatt karakter
					DECODER_state = 0; // ferdig, vent p� neste startbit
					} else {
					// ikke gyldig stoppbit,
					DECODER_state = 0; // ferdig, vent p� neste startbit
				}
			}
		}
		break;
		default:
		DECODER_state = 0;
		break;
	}
}

