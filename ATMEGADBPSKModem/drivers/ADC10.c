/*
 * ADC10.c
 *
 * Created: 07.02.2015 15:35:03
 *  Author: Kjell
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>

// Initialisering av ADC tatt fra eksempel p� internett. ADCen overklokkes for � kj�re p� 16e6/(13*32) = 38461.538 Hz. 
// Dette medf�rer at de 2 minst signifikante bitene blir d�rlige, men de brukes ikke likevell. Dette gj�r vi for � klare h�yere bitrate/f� bedre st�yimmunitet p� lavere bitrate

void ADC10_init(void)
{
	ADMUX |= _BV(ADLAR); // Sett at vi �nsker de 8 MSB i ADCH siden vi kun �nsker � bruke de
	
	// Set REFS1..0 in ADMUX (0x7C) to change reference voltage to the
	// proper source (01)
	ADMUX |= 0B01000000;
	
	// Clear MUX3..0 in ADMUX (0x7C) in preparation for setting the analog
	// input
	ADMUX &= 0B11110000;
	
	// Set MUX3..0 in ADMUX (0x7C) to read from AD8 (Internal temp)
	// Do not set above 15! You will overrun other parts of ADMUX. A full
	// list of possible inputs is available in Table 24-4 of the ATMega328
	// datasheet
	ADMUX |= 0;
	// ADMUX |= B00001000; // Binary equivalent
	
	// Set ADEN in ADCSRA (0x7A) to enable the ADC.
	// Note, this instruction takes 12 ADC clocks to execute
	ADCSRA |= 0B10000000;
	
	// Set ADATE in ADCSRA (0x7A) to enable auto-triggering.
	ADCSRA |= 0B00100000;
	
	// Clear ADTS2..0 in ADCSRB (0x7B) to set trigger mode to free running.
	// This means that as soon as an ADC has finished, the next will be
	// immediately started.
	ADCSRB &= 0B11111000;
	
	// Set the Prescaler to 32 (16000KHz/32 = 500 kHz)
	// Above 200KHz 10-bit results are not reliable.
	ADCSRA |= 0B00000101;
	
	// Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
	// Without this, the internal interrupt will not trigger.
	ADCSRA |= 0B00001000;
	
	// Enable global interrupts
	// AVR macro included in <avr/interrupts.h>, which the Arduino IDE
	// supplies by default.
	sei();
	
	// Set ADSC in ADCSRA (0x7A) to start the ADC conversion
	ADCSRA |= 0B01000000;
}
