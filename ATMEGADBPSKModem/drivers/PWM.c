/*
 * PWM.c
 *
 * Created: 10.02.2015 19:11:39
 *  Author: Kjell
 */ 

#include <avr/io.h>

// Still inn PWMen til � g� s� fort som mulig men ha 8 bits oppl�sning (alts� 256 niv�er).
// Dette gir en PWM frekvens p� 16e6/256 = 62.5 kHz. Denne frekvensen er s�pass mye h�yere enn signalet vi typisk skal sende (0-5 kHz typisk) at
// en kan bruke ett enkelt RC ledd for � f� ett fint signal.
void PWM_init(void)
{
	//DDRD |= (1 << DDD6); // Lag port D pinne 6 utgang
	OCR0A = 128; // Sett initielt 50% dutycycle
	TCCR0A |= (1<<COM0A1)|(0<<COM0A0)|(0<<WGM02)|(1<<WGM01)|(1<<WGM00); // Sett FAST PWM b�lgeformsgenereringsmodus da vi �nsker s� h�y som mulig svitsjefrekvens
	TCCR0B |= (1<<CS00);
}