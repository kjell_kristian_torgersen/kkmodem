#include <stdint.h>
#include <string.h>

#include <stdint.h>

#include "UART.h"
#include "packageComm.h"



uint8_t PC_package[PACKAGE_SIZE];
uint8_t PC_packageIndex = 0;
uint16_t PC_packageChecksum;

/*
void PC_transmitConfirmPackage(PackageValue_t value)
{
	uint16_t checksum = 0;
	UART_putchar((char)START_BYTE);
	checksum += (uint16_t)START_BYTE;
	UART_putchar(2);
	checksum += (2);
	UART_putchar((char)PT_CONFIRM);
	checksum += (uint16_t)PT_CONFIRM;
	UART_putchar((char)value);
	checksum += (uint16_t)value;
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t)(~checksum));
}


void PC_transmitMessage(char *message)
{
	//PC_transmitDataPackage(PV_MESSAGE, (uint8_t*)message, strlen(message));
}

void PC_transmitDataPackage(PackageValue_t value, uint8_t * payload, uint8_t length)
{
	int i;
	uint16_t checksum = 0;
	UART_putchar((char)START_BYTE);
	checksum += (uint16_t)START_BYTE;
	UART_putchar(length+2);
	checksum += (length+2);
	UART_putchar((char)PT_DATA);
	checksum += (uint16_t)PT_DATA;
	UART_putchar((char)value);
	checksum += (uint16_t)value;
	for (i = 0; i < length; i++) {
		UART_putchar(payload[i]);
		checksum += payload[i];
	}
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t)(~checksum));
}


void PC_transmitPackage(PackageType_t type, uint8_t * payload, uint8_t length) {
	int i;
	uint16_t checksum = 0;
	UART_putchar((char)START_BYTE);
	checksum += (uint16_t)START_BYTE;
	UART_putchar(length+1);
	checksum += length+1;
	UART_putchar((char)type);
	checksum += (uint16_t)type;
	for (i = 0; i < length; i++) {
		UART_putchar(payload[i]);
		checksum += payload[i];
	}
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t)(~checksum));
}



static void interpretSet(uint8_t *package)
{
	PackageValue_t pv = package[3];
	switch(pv)
	{
		case PV_PID:
		memcpy((void*)&heaterPID, (const void*)&package[4], sizeof(PID_t));
		PC_transmitConfirmPackage(PV_PID);
		break;
		case PV_AUTOTRANSMIT:
		autoTransmitPackages = package[4];
		PC_transmitConfirmPackage(PV_AUTOTRANSMIT);
		break;
		case PV_SETFLAG:
		{
			uint16_t mask;
			memcpy((void*)&mask, (const void*)&package[4], sizeof(uint16_t));
			flags |= mask;
			PC_transmitConfirmPackage(PV_SETFLAG);
		}
		break;
		case PV_CLEARFLAG:
		{
			uint16_t mask;
			memcpy((void*)&mask, (const void*)&package[4], sizeof(uint16_t));
			flags &= ~mask;
			PC_transmitConfirmPackage(PV_CLEARFLAG);
		}
		break;
		case PV_FLAG: // "dangerous", use with caution
		memcpy((void*)&flags, (const void*)&package[4], sizeof(uint16_t));
		PC_transmitConfirmPackage(PV_FLAG);
		break;
		case PV_SETPOINT:
		memcpy((void*)&heaterProcess.Sp, (const void*)&package[4], sizeof(float));
		PC_transmitConfirmPackage(PV_SETPOINT);
		break;
		case PV_RESETPROCESS:
		heaterProcess.I = 0;
		PC_transmitConfirmPackage(PV_RESETPROCESS);
		break;
		case PV_POWER:
		memcpy((void*)&POWER_timerValue, (const void*)&package[4], sizeof(uint16_t));
		PC_transmitConfirmPackage(PV_POWER);
		break;
		default:
		break;
	}
}

static void interpretGet(uint8_t *package)
{

	PackageValue_t pv = package[3];
	switch(pv)
	{
		case PV_PID:
	    PC_transmitDataPackage(PV_PID, (void*)&heaterPID, sizeof(PID_t));
		break;
		case PV_AUTOTRANSMIT:
		PC_transmitDataPackage(PV_AUTOTRANSMIT, (void*)&autoTransmitPackages, 1);
		break;
		case PV_FLAG:
		PC_transmitDataPackage(PV_FLAG, (void*)&flags, sizeof(uint16_t));
		break;
		case PV_SETPOINT:
		PC_transmitDataPackage(PV_SETPOINT, (void*)&heaterProcess.Sp, sizeof(float));
		break;
		case PV_POWER:
		PC_transmitDataPackage(PV_POWER, (void*)&POWER_timerValue, sizeof(uint16_t));
		break;
		default:
		break;
	}
}

static void interpretPackage(uint8_t *package) {
	PackageType_t pt = (PackageType_t) package[2];
	switch (pt) {
		case PT_GET: {
			interpretGet(package);
			//uint8_t payload[] = { 0, 1, 2, 3 };
			//transmitPackage(PT_DUMMY, payload, 4);
		}
		break;
		case PT_DATA: // ignore
		break;
		case PT_SET:
		interpretSet(package);
		break;
		case PT_CONFIRM:
		break;
	}
}
*/

void echoReceivedPackage(void);

void PC_processPackage(uint8_t b)
{
	switch (PC_packageIndex) {
		case 0:
		if (b == START_BYTE) {
			PC_package[0] = b;
			PC_packageIndex = 1;
			PC_packageChecksum = b;
		}
		break;
		case 1:
		PC_package[1] = b;
		PC_packageChecksum += b;
		PC_packageIndex = 2;
		break;
		default:
		PC_package[PC_packageIndex] = b;
		PC_packageChecksum += b;
		PC_packageIndex++;
		if ((PC_packageIndex >= PC_package[1] + 3) || PC_packageIndex >= PACKAGE_SIZE) {
			PC_packageIndex = 0;
			while (PC_packageChecksum > 0x100) {
				PC_packageChecksum = (PC_packageChecksum >> 8)
				+ (PC_packageChecksum & 0xFF);
			}
			PC_packageChecksum = (~PC_packageChecksum) & 0xFF;
			if (PC_packageChecksum == 0) {
				echoReceivedPackage();
				} else {
				//PC_transmitMessage("CS ERR");
			}
		}
		break;
	}
}
