/*
* ArduinoGccTest.c
*
* Created: 31.01.2015 16:15:14
*  Author: Kjell
*/


#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 16000000UL
#include <util/delay.h>


#include "global.h"
#include "headers/ADC10.h"
#include "headers/PWM.h"
#include "headers/timers.h"
#include "headers/packagecomm.h"
#include "headers/UART.h"
#include "headers/io-pins.h"

// For � kunne n� karakterer som modemet har demodulert
extern volatile signed int DECODER_rxchar;
extern uint8_t PC_package[PACKAGE_SIZE];
extern uint16_t new_tx_word;
uint8_t pidx = 0;
int TransmissionState = 0;
uint16_t txTimer=0;

void echoReceivedPackage()
{
	DDRD |= (1<<6); // skru p� sender
	TransmissionState = 1;
	UART_putchar('E');
}

void handleRepeater()
{
	switch(TransmissionState)
	{
		case 0: // wait for package
		UART_putchar('0');
		if(DECODER_rxchar != -1) {
			PC_processPackage(DECODER_rxchar);
			UART_putchar(DECODER_rxchar);
		}
		break;
		case 1:
		txTimer = TIMER_GetMilliSeconds();
		TransmissionState = 2;
		UART_putchar('1');
		break;
		case 2:
		if(TIMER_GetMilliSeconds() - txTimer > 100) {
			UART_putchar('2');
			TransmissionState = 3;
		}
		break;
		case 3:
		if(new_tx_word == -1) {
			UART_putchar('3');
			if(pidx < PC_package[1] + 3) {
				new_tx_word = 0xFF00; // Fyll med startbit, lag plass for mottatt byte som skal ut p� "lufta"
				new_tx_word |= ((PC_package[pidx]) & 0xFF); // Legg til byte mottatt fra HW uart
				new_tx_word <<= 1; // Lag plass for stoppbit
				new_tx_word &= ~0x1; // Stoppbit (denne linjen trengs kanskje ikke ...)
				pidx++;
				} else {
				pidx = 0;
				TransmissionState = 4;
				txTimer = TIMER_GetMilliSeconds();
			}
		}
		break;
		case 4:
		if(TIMER_GetMilliSeconds() - txTimer > 100) {
			UART_putchar('4');
			DDRD &= ~(1<<6);
			TransmissionState = 0;
		}
		break;
		default:
		break;
	}
}

void setTXWord(char c)
{
	cli();
	new_tx_word = 0xFF00; // Fyll med startbit, lag plass for mottatt byte som skal ut p� "lufta"
	new_tx_word |= ((c) & 0xFF); // Legg til byte mottatt fra HW uart
	new_tx_word <<= 1; // Lag plass for stoppbit
	new_tx_word &= ~0x1; // Stoppbit (denne linjen trengs kanskje ikke ...)
	sei();
}

void handleUART()
{
	int c;
	switch(TransmissionState)
	{
	case 0: // Vent til det er karakter i UART mottaksbuffer, start sender, men vent ...
		if(UART_peekchar() != -1) {
			MODEM_PTT_PORT |= MODEM_PTT_PIN;
			MODEM_TX_DDR |= MODEM_TX_PIN; // start � gi ut PWM signal
			txTimer = TIMER_GetMilliSeconds(); // start timer
			TransmissionState = 1; //
		}
	break;
	case 1:
		if(TIMER_GetMilliSeconds() - txTimer >= 500) { // La det g� 500 ms f�r vi gir inn f�rste karakter som skal sendes
			txTimer = TIMER_GetMilliSeconds(); // reset timer
			if(new_tx_word == -1) {
				c = UART_getchar();
				if(c != -1) {
					setTXWord(c);
					//UART_putchar(c);
				}
			}
			TransmissionState = 2;
		}
	break;
	case 2: // for inn nye bokstaver
		if(new_tx_word == -1) {
			c = UART_getchar(); // sjekk om det er flere karakter � sende?
			if(c != -1) { // ja
				txTimer = TIMER_GetMilliSeconds(); // reset timer
				setTXWord(c);
				//UART_putchar(c);
			} 
		}
		
		// dersom det ikke kommer nye karakterer i l�pet av 500 ms, stopp sender
		if(TIMER_GetMilliSeconds() - txTimer >= 500) {
			MODEM_PTT_PORT &= ~MODEM_PTT_PIN;
			MODEM_TX_DDR &= ~(MODEM_TX_PIN);
			TransmissionState = 0; // hopp tilbake for � vente p� ny karakter
		}
	break;
	}
}

int main(void)
{
	//#ifdef REPEATER
	
	//#endif
	// Lag Port D pinne 7 som utganger. Pinne D7 brukes for � m�le tiden ADC interruptet tar.
	DDRD |= (1<<7);
	
	// Sett PTT lav (h�y for sending)
	MODEM_PTT_PORT &= ~MODEM_PTT_PIN; 
	MODEM_PTT_DDR |= MODEM_PTT_PIN; // Sett PTT pinne som utgang
	
	// NB. Sett UART baud raten her til � matche (eller v�re h�yere enn) det som st�r i modem.c (Dersom h�yere, implementer flyttkontroll.)
	UART_init(1200);
	ADC10_init();
	PWM_init();
	TIMER_Init();
	
	//DDRD |= _BV(PORTD2);
	
	//myTimer = TIMER_GetMilliSeconds();
	// Evig l�kke, dersom modemet mottar data, send ut p� HW. UART til bruker.
	while(1)
	{
		/*if(TIMER_GetMilliSeconds() - myTimer >= 3000) {
		myTimer = TIMER_GetMilliSeconds();
		UART_putchar('.');
		}*/
		// Dersom UART_rxchar ikke er -1, s� har vi mottatt en karakter som kan sendes til bruker.
		if(DECODER_rxchar != -1) {
			//#ifndef REPEATER
			UART_putchar((char) DECODER_rxchar);
			//#endif
			DECODER_rxchar = -1;
		}
#ifdef REPEATER
		handleRepeater()
#else
		handleUART();
#endif
	}
}